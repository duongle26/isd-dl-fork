# README

## Lê Hải Dương

- Luồng sự kiện cho usecase one-way ticket

## Nguyễn Anh Đức

- Luồng sự kiện cho usecase enter/exit platform

## Nguyễn Nhật Hải

- Luồng sự kiện cho usecase prepaidcard

## Lã Ngọc Dương

- Luồng sự kiện cho usecase 24h ticket
