Reviewer: Lã Ngọc Dương 
Review for Lê Hải Dương
Content: Action quét thẻ, nhận dạng vé không nằm trong use case đang xét.

Reviewer: Nguyễn Anh Đức.
Review for: Lã Ngọc Dương.
Content:
1.	Check information of prepaid card
Hệ thống kiểm tra Card’s ID để biết về số dư trong Card và nhận về thông tin Trạm dừng chân hiện tại, qua đó tính toán xem số dư có đủ để đến được ga tiếp theo hay không. Nếu không sẽ báo lại và ngược lại thì mở cổng.
Nhận xét: Đây là phần Check info when enter the station platform trong Check information of prepaid card nên bạn mô tả chưa đúng.
2.	Check information of 24h ticket
Hệ thống nhận vào địa điểm sân ga và thời gian lần đầu sử dung của thẻ kiểm tra xem hiện tại đã vượt quá 24 h chưa để biết vé đó có hợp lệ hay không.
Nhận xét: Đối với vé 24h việc lấy thông tin về địa điểm sân ga là không quá cần thiết. và “System” không làm công việc “Open” mà truyền lệnh cho“Gate” thực hiện nó nên ở đây bạn mô tả chưa chính xác về Check information of 24h ticket.

Reviewer Nguyễn Nhật Hải.
Review for Nguyễn Anh Đức.
Content:
-    Scan card không có Alternative flow of events thì nên xóa đi. 
-    Cần viết rõ Return card or ticket

Reviewer: Lê Hải Dương
Review for Nguyễn Nhật Hải
Content:
_ Thiếu sơ đồ use case diagram ở đầu bài
_ Cách viết flow event chưa đúng form mà đề bài yêu cầu (Theo bảng)
_ Nội dung khá đầy đủ