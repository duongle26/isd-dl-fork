Reviewer: Nguyễn Nhật Hải
Review for Lê Hải Dương
Content:
- Hoàn thiện các phần còn lại trong báo cáo đầy đủ.
- Phần đặc tả usecase UC006 "Gate" không có luồng thay thế thì nên bỏ đi.
- Đặc tả các usecase còn lại đầy đủ nội dung.
