package view;

import controller.*;
import hust.soict.se.customexception.InvalidIDException;
import model.*;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * The Class UI.
 */
public class UI {
	
	/** The instance. */
	private static UI instance = new UI();
	
	/** The message. */
	private String message = null;
	
	/** The type. */
	private String type;
	
	/** The id. */
	private String id;
	
	/** The info. */
	private String info;
	
	/** The error. */
	private String error = null;

	/**
	 * Gets the single instance of UI.
	 *
	 * @return single instance of UI
	 */
	public static UI getInstance() {
		return instance;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Sets the info.
	 *
	 * @param info the new info message
	 */
	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * Sets the error.
	 *
	 * @param error the new error message
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * Show menu.
	 *
	 * @throws InvalidIDException the invalid ID exception
	 * @throws SQLException there was a problem querying the database
	 */
	public void showMenu() throws InvalidIDException, SQLException {
		int actionChoice = 0;
		int stationChoice;
		int methodChoice;
		int indexChoice;
		String barCode = null;
		PlatformController platform = PlatformController.getInstance();
		StationController station = StationController.getInstance();
		OneWayTicketController oneWayTicket = OneWayTicketController.getInstance();
		Ticket24hController ticket24h = Ticket24hController.getInstance();
		PrepaidCardController prepaidCard = PrepaidCardController.getInstance();

		String[] method = {"One-way Ticket", "24h Ticket", "Prepaid Card"};

		while (true) {
			System.out.println("These are stations in the line M14 of Paris:");
			for (Station sta : station.getListStation()) {
				System.out.println(sta.getId() + ". " + sta.getName());
			}
			System.out.println("\nAvailable actions: 1 - Enter station, 2 - Exit station\n");
			actionChoice = inputHandle(1, 2, "Your Action: ", "Invalid Option !");
			stationChoice = inputHandle(1, station.getListStation().size(), "Your Station: ", "Invalid Station !");

			System.out.println("\nChoose type of ticket/card: ");
			for (int i = 1; i <= method.length; i++) {
				System.out.println(i + ". " + method[i - 1]);
			}
			methodChoice = inputHandle(1, 3, "Your choice: ", "Invalid Option !");

			switch (methodChoice) {
				case 1:
					System.out.println("\nThese are existing One-way Tickets:");
					for (OneWayTicket ticket : oneWayTicket.getListOneWayTicket()) {
						System.out.println("- " + ticket.getBarCode() + ": One-way Ticket between " + station.getStationName(ticket.getEmbarkStationId())
								+ " and " + station.getStationName(ticket.getDisembarkStationId()) + ": " + ticket.getStatus() + ": " + ticket.getFare() + " euros");
					}
					indexChoice = inputHandle(1, oneWayTicket.getListOneWayTicket().size(), "Your choice: ", "Invalid Option !");
					barCode = oneWayTicket.getListOneWayTicket().get(indexChoice - 1).getBarCode();
					this.type = "One-way Ticket";
					this.id = oneWayTicket.getListOneWayTicket().get(indexChoice - 1).getId();
					this.info = "One-way Ticket between " + station.getStationName(oneWayTicket.getListOneWayTicket().get(indexChoice - 1).getEmbarkStationId()) + " and "
							+ station.getStationName(oneWayTicket.getListOneWayTicket().get(indexChoice - 1).getDisembarkStationId());
					break;
				case 2:
					System.out.println("\nThese are existing 24h Tickets:");
					for (Ticket24h ticket : ticket24h.getListTicket24h()) {
						System.out.print("- " + ticket.getBarCode() + ": 24h Ticket: " + ticket.getStatus());
						if (ticket.getExpireTime() != null) {
							System.out.println(": Valid until " + ticket.getExpireTime());
						} else System.out.println(": New");
					}
					indexChoice = inputHandle(1, ticket24h.getListTicket24h().size(), "Your choice: ", "Invalid Option !");
					barCode = ticket24h.getListTicket24h().get(indexChoice - 1).getBarCode();
					this.type = "24h Ticket";
					this.id = ticket24h.getListTicket24h().get(indexChoice - 1).getId();
					this.info = "Valid until " + ticket24h.getListTicket24h().get(indexChoice - 1).getExpireTime();
					break;
				case 3:
					System.out.println("\nThese are existing Prepaid Cards:");
					for (PrepaidCard card : prepaidCard.getListPrepaidCard()) {
						System.out.println("- " + card.getBarCode() + ": Prepaid Card: " + card.getStatus() + ": " + card.getBalance() + " euros");
					}
					indexChoice = inputHandle(1, prepaidCard.getListPrepaidCard().size(), "Your choice: ", "Invalid Option !");
					barCode = prepaidCard.getListPrepaidCard().get(indexChoice - 1).getBarCode();
					this.type = "Prepaid Card";
					this.id = prepaidCard.getListPrepaidCard().get(indexChoice - 1).getId();
					this.info = "Balance: " + prepaidCard.getListPrepaidCard().get(indexChoice - 1).getBalance() + " euros";
					break;
			}
			switch (actionChoice) {
				case 1:
					platform.enterStation(stationChoice, methodChoice, barCode);
					this.displayMessage();
					break;

				case 2:
					platform.exitStation(stationChoice, methodChoice, barCode);
					this.displayMessage();
					break;
			}
		}
	}

	/**
	 * Input handle.
	 *
	 * @param start the start number
	 * @param end the end number
	 * @param message the message
	 * @param error the error message
	 * @return the user's choice
	 */
	public int inputHandle(int start, int end, String message, String error) {
		Scanner scanner = new Scanner(System.in);
		boolean validInput;
		int choice = 0;
		do {
			System.out.print(message);
			try {
				choice = scanner.nextInt();
				if (choice < start || choice > end) {
					validInput = true;
					System.out.println(error);
				} else validInput = false;
			} catch (InputMismatchException e) {
				System.out.println(error);
				scanner.next();
				validInput = true;
			}
		} while (validInput);
		return choice;
	}

	/**
	 * Display message.
	 */
	public void displayMessage() {
		Scanner scanner = new Scanner(System.in);
		if (this.error != null) System.out.println(this.message);
		System.out.println("Type: " + this.type);
		System.out.println("ID: " + this.id);
		System.out.println(this.info);
		if (this.error != null) System.out.println(this.error);
		System.out.println("\nPress enter to continue...");
		scanner.nextLine();
	}
}