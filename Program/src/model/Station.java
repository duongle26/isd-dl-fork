package model;

/**
 * The Class Station.
 */
public class Station {

	/** The id. */
	private int id;
	
	/** The name. */
	private String name;
	
	/** The distance. */
	private float distance;

	/**
	 * Instantiates a new station.
	 *
	 * @param id the id
	 * @param name the name
	 * @param distance the distance
	 */
	public Station(int id, String name, float distance) {
		this.id = id;
		this.name = name;
		this.distance = distance;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the distance.
	 *
	 * @return the distance
	 */
	public float getDistance() {
		return this.distance;
	}
}