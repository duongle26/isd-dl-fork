package model;

import java.sql.Timestamp;

/**
 * The Class Ticket24h.
 */
public class Ticket24h {

	/** The id. */
	private String id;
	
	/** The code. */
	private String code;
	
	/** The bar code. */
	private String barCode;
	
	/** The expire time. */
	private Timestamp expireTime;
	
	/** The status. */
	private String status;

	/**
	 * Instantiates a new ticket 24 h.
	 *
	 * @param id the id
	 * @param code the code
	 * @param barCode the bar code
	 * @param expireTime the expire time
	 * @param status the status
	 */
	public Ticket24h(String id, String code, String barCode, Timestamp expireTime, String status) {
		this.id = id;
		this.code = code;
		this.barCode = barCode;
		this.expireTime = expireTime;
		this.status = status;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Gets the bar code.
	 *
	 * @return the bar code
	 */
	public String getBarCode() {
		return barCode;
	}

	/**
	 * Gets the expire time.
	 *
	 * @return the expire time
	 */
	public Timestamp getExpireTime() {
		return expireTime;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
}